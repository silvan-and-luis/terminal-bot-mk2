# Terminal Bot Mk2

Simple bot for the game "Terminal" by Correlation One, written in Python.  
This bot takes an even more dynamic/reactive approach when it comes to defense than its predecessor to prevent spending resources on defenses that are not needed.  
The code changes proposed in Merge Requests need to be evaluated by uploading the code of the given branch to Terminal and letting it run there for a while.  
Only Merge Requests which improve upon the current rating of the master-algo and MRs which don't change the rating will be merged.  
This project is finished, it could sadly not beat its predecessor regarding the ranking.  

[Terminal homepage](https://terminal.c1games.com/)  
[Tool for converting graphically selected tiles to coordinates](https://www.kevinbai.design/terminal-map-maker/)  

## Possible error when uploading an algo to Terminal

When uploading an algo to Terminal, the following error can occur:  
"/bin/sh: 1: ./run.sh: not found"  

It is caused by Unix line breaks being replaced by DOS line breaks.  
The error can be resolved by using a converter to replace the line endings in "run.sh".  
["dos2unix"](https://sourceforge.net/projects/dos2unix/) can be used for the conversion.  

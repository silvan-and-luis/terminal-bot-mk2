import gamelib
import random
import math
import warnings
from sys import maxsize
import json


class AlgoStrategy(gamelib.AlgoCore):

    def __init__(self):
        super().__init__()
        seed = random.randrange(maxsize)
        random.seed(seed)
        gamelib.debug_write('Random seed: {}'.format(seed))
        self.scored_last_turn = False
        self.enemy_edge_locations = None
        self.game_state = None
        self.used_demolishers_last_round = None
        self.enemy_used_funnel = False
        self.min_demolishers_per_wave = 3
        self.friendly_edge_locations = [[0, 13], [1, 12], [2, 11], [3, 10], [4, 9], [5, 8], [6, 7], [7, 6], [8, 5], [9, 4], [10, 3], [11, 2], [12, 1], [13, 0], [14, 0], [15, 1], [16, 2], [17, 3], [18, 4], [19, 5], [20, 6], [21, 7], [22, 8], [23, 9], [24, 10], [25, 11], [26, 12], [27, 13]]
        self.corner_wall_locations = [[0, 13], [27, 13], [1, 12], [26, 12], [2, 11], [25, 11], [3, 10], [24, 10]]
        self.not_directly_defendable_locations = [[0, 13], [1, 12], [2, 11], [3, 10], [12, 1], [13, 0], [14, 0], [15, 1], [24, 10], [25, 11], [26, 12], [27, 13]]
        self.defense_compound_indices_to_friendly_edge_location_indices = {
            0: [4, 5, 6, 7],
            1: [8, 9, 10, 11],
            2: [16, 17, 18, 19],
            3: [20, 21, 22, 23]
        }
        self.defense_compound_indices_to_turret_locations = {
            0: [[5, 8], [6, 9], [6, 7], [7, 8]],
            1: [[9, 4], [10, 5], [10, 3], [11, 4]],
            2: [[16, 4], [17, 3], [17, 5], [18, 4]],
            3: [[20, 8], [21, 7], [21, 9], [22, 8]]
        }
        self.defense_compound_indices_to_wall_locations = {
            0: [[5, 9], [6, 10], [7, 9], [8, 8], [7, 7]],
            1: [[9, 5], [10, 6], [11, 5], [12, 4]],
            2: [[15, 4], [16, 5], [17, 6], [18, 5]],
            3: [[20, 7], [19, 8], [20, 9], [21, 10], [22, 9]]
        }
        self.fallback_turret_locations = [[13, 8], [14, 8], [7, 9], [8, 9], [19, 9], [20, 9], [4, 10], [23, 10]]
        self.fallback_wall_locations = [[13, 9], [14, 9], [7, 10], [8, 10], [19, 10], [20, 10], [4, 11], [23, 11]]
        self.support_locations = [[14, 0], [12, 1], [14, 1]]
        self.additional_support_locations = [[15, 1], [12, 2], [14, 2], [15, 2]]
        self.spawn_location = [13, 0]

    # Read in config and perform any initial setup here
    def on_game_start(self, config):
        gamelib.debug_write('Configuring the algo ...')
        self.config = config
        global WALL, SUPPORT, TURRET, SCOUT, DEMOLISHER, INTERCEPTOR, MP, SP
        WALL = config["unitInformation"][0]["shorthand"]
        SUPPORT = config["unitInformation"][1]["shorthand"]
        TURRET = config["unitInformation"][2]["shorthand"]
        SCOUT = config["unitInformation"][3]["shorthand"]
        DEMOLISHER = config["unitInformation"][4]["shorthand"]
        INTERCEPTOR = config["unitInformation"][5]["shorthand"]
        MP = 1
        SP = 0

    """
    This method is called every turn with the game state wrapper as an argument.
    The wrapper stores the state of the arena and has methods for querying its state and allocating the current resources as planned
    """
    def on_turn(self, turn_state):
        self.game_state = gamelib.GameState(self.config, turn_state)
        gamelib.debug_write('Performing turn {} of your custom algo strategy'.format(self.game_state.turn_number))
        # game_state.suppress_warnings(True)
        if self.enemy_edge_locations == None:
            self.enemy_edge_locations = self.game_state.game_map.get_edge_locations(self.game_state.game_map.TOP_LEFT) + self.game_state.game_map.get_edge_locations(self.game_state.game_map.TOP_RIGHT)
        self.execute_strategy()
        self.scored_last_turn = False
        self.game_state.submit_turn()

    # This method can be called hundreds of times per turn and could slow the algo down so avoid putting slow code here
    def on_action_frame(self, turn_string):
        state = json.loads(turn_string)
        events = state["events"]
        breaches = events["breach"]
        for breach in breaches:
            location = breach[0]
            unit_owner_self = True if breach[4] == 1 else False
            # 1 is associated with us, 2 is associated with our enemy (StarterKit code uses 0, 1 as player_index instead)
            if unit_owner_self:
                self.scored_last_turn = True

    def execute_strategy(self):
        # Protect front corners with walls
        self.game_state.attempt_spawn(WALL, self.corner_wall_locations)

        reachable_edge_locations = self.get_own_reachable_edge_locations()
        defense_compound_indices = self.get_needed_defense_compounds(reachable_edge_locations)
        # If we can protect all edge locations the enemy can reach with two defense compounds or less
        if len(defense_compound_indices) <= 2:
            self.enemy_used_funnel = True
            for defense_compound_index in defense_compound_indices:
                self.build_defense_compound(defense_compound_index)

        if self.game_state.turn_number >= 3 and self.enemy_used_funnel == False:
            self.game_state.attempt_spawn(TURRET, self.fallback_turret_locations)
            self.game_state.attempt_spawn(WALL, self.fallback_wall_locations)
            self.game_state.attempt_upgrade(self.fallback_turret_locations)
            self.game_state.attempt_upgrade(self.fallback_wall_locations)

        self.game_state.attempt_spawn(SUPPORT, self.support_locations)
        if self.game_state.turn_number >= 3:
            self.game_state.attempt_spawn(SUPPORT, self.additional_support_locations)

        if self.used_demolishers_last_round == True and self.scored_last_turn == False:
            if self.min_demolishers_per_wave + 1 <= self.calculate_max_saveable_mp_count() // self.game_state.type_cost(DEMOLISHER)[1]:
                self.min_demolishers_per_wave += 1

        if self.scored_last_turn == True:
            self.game_state.attempt_spawn(SCOUT, self.spawn_location, 1000)
            self.used_demolishers_last_round = False
        elif self.game_state.number_affordable(DEMOLISHER) >= self.min_demolishers_per_wave:
            self.game_state.attempt_spawn(DEMOLISHER, self.spawn_location, 1000)
            self.used_demolishers_last_round = True
        else:
            self.used_demolishers_last_round = False

    def get_own_reachable_edge_locations(self):
        reachable_locs = []
        for location in self.enemy_edge_locations:
            path = self.game_state.find_path_to_edge(location)
            if not path == None and path[-1] in self.friendly_edge_locations:
                reachable_locs.append(path[-1])
        reachable_locs = sorted(reachable_locs)
        return [reachable_locs[i] for i in range(len(reachable_locs)) if i == 0 or reachable_locs[i] != reachable_locs[i-1]]

    def get_needed_defense_compounds(self, edge_locations):
        needed_defense_compound_indices = []
        for location in edge_locations:
            location_index = self.friendly_edge_locations.index(location)
            for defense_compound_index in self.defense_compound_indices_to_friendly_edge_location_indices.keys():
                if location_index in self.defense_compound_indices_to_friendly_edge_location_indices[defense_compound_index]:
                    needed_defense_compound_indices.append(defense_compound_index)
        return needed_defense_compound_indices

    def build_defense_compound(self, defense_compound_index):
        turret_locations = self.defense_compound_indices_to_turret_locations[defense_compound_index]
        self.game_state.attempt_spawn(TURRET, turret_locations)
        wall_locations = self.defense_compound_indices_to_wall_locations[defense_compound_index]
        self.game_state.attempt_spawn(WALL, wall_locations)
        self.game_state.attempt_upgrade(turret_locations)
        self.game_state.attempt_upgrade(wall_locations)

    def calculate_max_saveable_mp_count(self):
        return (self.game_state.turn_number // 10 + 5) * 4

if __name__ == "__main__":
    algo = AlgoStrategy()
    algo.start()
